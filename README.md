# python_simbicon

This project was created to make the code of the Generalized biped controller up to date.
Here is a link to the original project page: http://www.cs.ubc.ca/~van/papers/2010-TOG-gbwc/index.html
The first commit of this repository contains the project working with the following components:
msvc14.0
python3.6 32 bits
wxpython4.0.1
pyopengl3.1.2
swigwin 3.0.12
freeglut3.0.0
glew

!!!WARNING!!! This project require having both visual studio 2015 and 2017 (both in the same language, trying with english).
The reason is that python need visual 2015 to be able to compile wx python you need to use a visual studio 2015 installation (it was not working with the 14.0 tools from the 2017 version). However to be able to use the mixed mode debugging you need to use visual studio 2017 (you need to make sure that each project is using the visual sution 2014 (140) toolset for compilation).

Note: You have to run cmake to configure ODE before compilling the complete solution.

Note that the lib containing python, freeglut and swig is in an archive at the root of the repository (lib.zip file), just unzip it next to the cartwheel-3d folder 

Just a a note if using your own python/swig is preferable (or if the lib.zip file is not accesible).
This zip simple contains the lib folder which contains
lib\
  Python36\
  swigwin-3.0.12\
  *the various lib and dll for freeglut and glew*

Be carefull to be able to use the mixed mode debugging, the use of python36_d.exe was needed (or at least I was succesfull by using it :) ). 
As such wxpython needs to be manualy compiled in debug config. To do it, download the sources (https://pypi.python.org/pypi/wxPython), open a cmd, go to the folder and type the following : (i know that the option debug+release exists, but since this solution worked I did not bother any further: 
python.exe .\build.py build --release 
python.exe .\build.py install
python.exe .\build.py build --debug 
python.exe .\build.py install

swig needs no install just download and unzip it (http://www.swig.org/download.html the swigwin one).

To get a working pyopengl, I had to use the .whl file given here https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyopengl
note: I did not install PyOpenGL_accelerate maybe it could be interesting for better performances.
