from itertools import islice
from random import random
from time import perf_counter

COUNT = 5000  # Change this value depending on the speed of your computer
DATA = list(islice(iter(lambda: (random() - 0.5) * 3.0, None), COUNT))

e = 2.7182818284590452353602874713527

def sinh(x):
    return (1 - (e ** (-2 * x))) / (2 * (e ** -x))

def cosh(x):
    return (1 + (e ** (-2 * x))) / (2 * (e ** -x))

def tanh(x):
    tanh_x = sinh(x) / cosh(x)
    return tanh_x

def sequence_tanh(data):
    '''Applies the hyperbolic tangent function to map all values in
    the sequence to a value between -1.0 and 1.0.
    '''
    result = []
    for x in data:
        result.append(tanh(x))
    return result

def test(fn, name):
    start = perf_counter()
    result = fn(DATA)
    duration = perf_counter() - start
    print('{} took {:.3f} seconds\n\n'.format(name, duration))

    for d in result:
        assert -1 <= d <= 1, " incorrect values"

if __name__ == "__main__":
    print('Running benchmarks with COUNT = {}'.format(COUNT))
    test(sequence_tanh, 'sequence_tanh')

    #test(lambda d: [tanh(x) for x in d], '[tanh(x) for x in d]')

    #check util lib
    if False:
        import Utils
        x = Utils.Image(4,10,10,None)
        bval = x.getBPixelAt(4,6)
        print(bval)

    #check mathlib
    if False:
        import MathLib
        x = MathLib.Vector3d()
        x.setValues(10,0.44,-132)
        print("x = ",x.x)
        y = MathLib.Vector3d()
        y.setValues(3,11,2)
        x.addScaledVector(y,0.5)
        print("x + 0.5y = ",x.x)

    #check GLUtils
    if False:
        import MathLib, GLUtils
        x = GLUtils.GLCamera()
        x.setTarget(MathLib.Point3d(2,3,1))
        x.applyCameraTransformations();

    # check Physics
    if False:
        import MathLib, Physics
        x= Physics.RigidBody()
        x.setCMPosition(MathLib.Point3d(1,2,3))
        x.setOrientation(3.14/4.0,MathLib.Vector3d(1,0,0)) 
        y=x.getWorldCoordinates(MathLib.Point3d(0,2,0))
        print(y.x,"  ",y.y,"  ",y.z)

    # check Core
    if True:
        import MathLib, Core
        x=Core.TrajectoryComponent()
        x.baseTraj=MathLib.Trajectory1d()
        x.getBaseTrajectory().addKnot(0,0)
        x.getBaseTrajectory().addKnot(0.5,1)
        x.getBaseTrajectory().addKnot(0.4,0.9)
        print(x.getBaseTrajectory().evaluate_catmull_rom(0.2))

    #check pygl
    if True:
        import OpenGL.GL
        from OpenGL.GL import *
        from OpenGL.GLU import *



