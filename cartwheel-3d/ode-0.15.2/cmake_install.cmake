# Install script for directory: D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/ODE")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/Debug/ode_doubled.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/Release/ode_double.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/MinSizeRel/ode_double.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/RelWithDebInfo/ode_double.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xruntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/Debug/ode_doubled.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/Release/ode_double.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/MinSizeRel/ode_double.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/RelWithDebInfo/ode_double.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/Debug/ode_doubled.pdb")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/RelWithDebInfo/ode_double.pdb")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ode" TYPE FILE FILES
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/collision.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/collision_space.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/collision_trimesh.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/common.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/compatibility.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/contact.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/cooperative.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/error.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/export-dif.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/mass.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/matrix.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/matrix_coop.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/memory.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/misc.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/objects.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/ode.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/odeconfig.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/odecpp.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/odecpp_collision.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/odeinit.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/odemath.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/odemath_legacy.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/rotation.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/threading.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/threading_impl.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/timer.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/precision.h"
    "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/include/ode/version.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/ode.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/ode-config")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/ode-config.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/ode-config-version.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2/ode-export.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2/ode-export.cmake"
         "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/CMakeFiles/Export/lib/cmake/ode-0.15.2/ode-export.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2/ode-export-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2/ode-export.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/CMakeFiles/Export/lib/cmake/ode-0.15.2/ode-export.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/CMakeFiles/Export/lib/cmake/ode-0.15.2/ode-export-debug.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/CMakeFiles/Export/lib/cmake/ode-0.15.2/ode-export-minsizerel.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/CMakeFiles/Export/lib/cmake/ode-0.15.2/ode-export-relwithdebinfo.cmake")
  endif()
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/ode-0.15.2" TYPE FILE FILES "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/CMakeFiles/Export/lib/cmake/ode-0.15.2/ode-export-release.cmake")
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "D:/Users/scarensac/Documents/doctorat/projet_2010_python_36_32_2015/cartwheel-3d/ode-0.15.2/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
